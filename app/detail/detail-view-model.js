var observableModule = require("data/observable");
const httpModule = require("http");
var observableArray = require("data/observable-array");
var chatList = new observableArray.ObservableArray([]);
var observableModule = require("data/observable");
var pageData = new observableModule.Observable();


function HomeViewModel() {
  var viewModel = observableModule.fromObject({
    onButtonTap: function (args) {
      console.log("Button was pressed");
      httpModule.request({
        url: "https://rest.messengerpeople.com/api/channel/chats?apikey=1790bb81cc54f90a039865bb8fd9fe34_4593_4426851a90aaade38bfc5ae71f&limit=50&start=&incoming=1&outgoing=1&bot=0",
        method: "GET"
      }).then((response) => {
        // Content property of the response is HttpContent
        // The toString method allows you to get the response body as string.
        const str = response.content.toString();
        // The toJSON method allows you to parse the received content to JSON object
        var obj = response.content.toJSON();
        chatList.length = 0;
        //console.log(Object.keys(obj["chats"]).length);
        for (i = 0; i < Object.keys(obj["chats"]).length; i++){
          var chat = { user_id: obj["chats"][i].user_id, chat: obj["chats"][i].chat };
          chatList.push(chat);
          console.log(chat.user_id);
          //console.log(i);
        }
        //console.log("chat list count " + chatList.length);
        })
      var page = args.object;
      pageData.set("chatList", chatList);
      // it links an xml "beerList" variable to a js beerList variable
      page.bindingContext = pageData;

    },

    countries: [
      { name: "Australia", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/au.png" },
      { name: "Belgium", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/be.png" },
      { name: "Bulgaria", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/bg.png" },
      { name: "Canada", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/ca.png" },
      { name: "Switzerland", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/ch.png" },
      { name: "China", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/cn.png" },
      { name: "Czech Republic", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/cz.png" },
      { name: "Germany", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/de.png" },
      { name: "Spain", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/es.png" },
      { name: "Ethiopia", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/et.png" },
      { name: "Croatia", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/hr.png" },
      { name: "Hungary", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/hu.png" },
      { name: "Italy", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/it.png" },
      { name: "Jamaica", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/jm.png" },
      { name: "Romania", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/ro.png" },
      { name: "Russia", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/ru.png" },
      { name: "United States", imageSrc: "https://play.nativescript.org/dist/assets/img/flags/us.png" },
    ],

 

    onItemTap: function (args) {
      console.log('Item with index: ' + args.index + ' tapped');

      httpModule.request({
        url: "https://rest.messengerpeople.com/api/channel/chats?apikey=1790bb81cc54f90a039865bb8fd9fe34_4593_4426851a90aaade38bfc5ae71f&limit=50&start=&incoming=1&outgoing=1&bot=0",
        method: "GET"
      }).then((response) => {

        for (var i = 0; i < response["chats"].length; i++)
        {

        
      }
        // Content property of the response is HttpContent
        // The toString method allows you to get the response body as string.
        const str = response.content.toString();
        // The toJSON method allows you to parse the received content to JSON object
        var obj = response.content.toJSON();
        //console.log(JSON.stringify(obj));
       
        viewModel.countries = array.fromObject(obj);


        console.log(viewModel.countries);
        // The toImage method allows you to get the response body as ImageSource.
        // var img = response.content.toImage();
      }, (e) => {
      });



    },

      
  });
 
  return viewModel;
}

module.exports = HomeViewModel;
