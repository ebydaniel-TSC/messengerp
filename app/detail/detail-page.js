/*
In NativeScript, a file with the same name as an XML file is known as
a code-behind file. The code-behind is a great place to place your view
logic, and to set up your page’s data binding.
*/

/*
NativeScript adheres to the CommonJS specification for dealing with
JavaScript modules. The CommonJS require() function is how you import
JavaScript modules defined in other files.
*/

require("globals");


var frameModule = require("ui/frame");
var HomeViewModel = require("./detail-view-model");
var observableArray = require("data/observable-array");
var chatList = new observableArray.ObservableArray([]);
var httpModule = require("http");
var observableModule = require("data/observable");
var pageData = new observableModule.Observable();


var dialogs = require("ui/dialogs");
var appSettings = require('application-settings');

var gluser;
var itemselected;
var apikey;

function pageLoaded(args) {
  /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */
  var page = args.object;

  //console.log("loaded...");

  if(!appSettings.hasKey('apikey'))
  {
      dialogs.prompt("Please enter your API Key","Please get your API key from MessengerPeople console").then(function(r){
        appSettings.setString('apikey',r.text);
      });

  }
  else
  {
    apikey = appSettings.getString('apikey');
  }


  loaddata();


  var scroll = page.getViewById("scroller");
  scroll.scrollToVerticalOffset(400,false);
  console.log(scroll.scrollableHeight);

  pageData.set("chatList", chatList);
  // it links an xml "beerList" variable to a js beerList variable
  page.bindingContext = { chatList };

  /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and JavaScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
  //page.bindingContext = homeViewModel;
}
function onItemTap (args) {
  //console.log("Item Tapped");
}
function onButtonTap (args) {
  //console.log("Button Tapped");
  //dialogs.alert("Back");
  //console.log("App settings " + appSettings.getString('selectedid'));
  const button = args.object;
  const page = button.page;
  page.frame.navigate("./home/home-page");


}
function onNavigatingTo(args) {
  const page = args.object;
  // You can access `info` property from the navigationEntry
  var context = page.navigationContext;
  /*console.log(context.info);
  console.log( appSettings.getString('selectedid'));*/
}
function onSndButtonTap(args){

  console.log("Sending ");
  var btn = args.object;
  var page = btn.page;
  var txt = page.getViewById("txtmessage");

  //var chat = { user_id: gluser, chat: txt.text,bkcolor:"background-color: PaleGreen;color: #4d4d4d;margin: 15 15 0;",chattime:"now" };

  //chatList.push(chat);
  
  
  
  //page.getViewById("scroller").scrollToVerticalOffset( page.getViewById("scroller").scrollableHeight,false);
  // send to whatsbroadcast
  apikey = appSettings.getString('apikey');

  var vcontent = "apikey="+apikey+"&id="+itemselected+"&message="+txt.text+"&push=1";

  txt.text="";

  httpModule.request({
    url: "https://rest.messengerpeople.com/api/chat/?"+vcontent,
    method: "POST",
    content: vcontent
  }).then((response) => {
      //console.log(response);
      loaddata();
  })

  

}

function scrollChatToBottom() {
  setTimeout(() => {
    this.chatBox.scrollToIndex(this.chatList.length - 1);
  }, 0);
}
function loaddata()
{

  itemselected = appSettings.getString('selectedid');

  chatList.length=0;


  var apiurl = 'https://rest.messengerpeople.com/api/channel/chats?apikey='+apikey+'&id='+itemselected+'&limit=50&showstartstop=1'
  
  httpModule.request({
    url: apiurl,
    method: "GET"
  }).then((response) => {
    // Content property of the response is HttpContent
    // The toString method allows you to get the response body as string.
    const str = response.content.toString();
    // The toJSON method allows you to parse the received content to JSON object
    var obj = response.content.toJSON();
    chatList.length = 0;
    //console.log(Object.keys(obj["chats"]).length);
    for (i = 0; i < Object.keys(obj["chats"]).length; i++) {
      var cdate = new Date();
      cdate.setSeconds(obj["chats"][i].chattime);
      var bgcolor;
      if(obj["chats"][i].outgoing==1)
        bgcolor="background-color: PaleGreen;color: #4d4d4d;margin: 15 15 0;";
      else
        bgcolor="background-color: #fff;color: #4d4d4d;margin: 15 15 0;"

      var chat = { user_id: obj["chats"][i].user_id, chat: obj["chats"][i].chat, chattime: cdate, outgoing: obj["chats"][i].outgoing, bkcolor: bgcolor };
      gluser = obj["chats"][i].user_id;
      //console.log(chat.user_id);
      
       if(itemselected == chat.user_id)
        {

            chatList.push(chat);
            gluser = chat.user_id;
        }

      
    }


    //console.log(chatList.getItem(i).user_id);
    //console.log("chat list count " + chatList.length);
  })

}
function onNavBtnTap(args)
{
  var obj= args.object;
  var page = obj.page;
  page.frame.navigate("./home/home-page");

}

/*
Exporting a function in a NativeScript code-behind file makes it accessible
to the file’s corresponding XML file. In this case, exporting the pageLoaded
function here makes the pageLoaded="pageLoaded" binding in this page’s XML
file work.
*/
exports.onNavigatingTo = onNavigatingTo;
exports.pageLoaded = pageLoaded;
exports.onItemTap = onItemTap;
exports.onButtonTap = onButtonTap;
exports.onSndButtonTap = onSndButtonTap;
exports.scrollChatToBottom= scrollChatToBottom;
exports.onNavBtnTap = onNavBtnTap;


