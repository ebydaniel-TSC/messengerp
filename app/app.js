/*
In NativeScript, the app.js file is the entry point to your application.
You can use this file to perform app-level initialization, but the primary
purpose of the file is to pass control to the app’s first module.
*/

var application = require("application");
var firebase = require("nativescript-plugin-firebase");
var dialogs = require("ui/dialogs");
var appSettings = require('application-settings');


firebase.init({
    // Optionally pass in properties for database, authentication and cloud messaging,
    // see their respective docs.
    onMessageReceivedCallback: function(message) {
        //dialogs.alert("Title: "+ message.title + "Body: "+ message.body);
        console.log("Title: " + message.title);
        console.log("Body: " + message.text);
        // if your server passed a custom property called 'foo', then do this:
        console.log("Value of 'foo': " + message.data.foo);
        
      },
      onPushTokenReceivedCallback: function(token) {
        //console.log("Firebase push token: " + token);
      },
      
  }).then(
      function (instance) {
        console.log("firebase.init done");
        if(appSettings.hasKey('apikey'))
        {
          apikey = appSettings.getString('apikey');
      
          firebase.subscribeToTopic(apikey).then(() => console.log("Subscribed to topic"));
          console.log(apikey);
          
        }
 
      },
      function (error) {
        //console.log("firebase.init error: " + error);
      }
  );



application.run({ moduleName: "app-root" });

//application.start({ moduleName: "app-root" });

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
