/*
In NativeScript, a file with the same name as an XML file is known as
a code-behind file. The code-behind is a great place to place your view
logic, and to set up your page’s data binding.
*/

/*
NativeScript adheres to the CommonJS specification for dealing with
JavaScript modules. The CommonJS require() function is how you import
JavaScript modules defined in other files.
*/

require("globals");


var frameModule = require("ui/frame");
var HomeViewModel = require("./home-view-model");
var observableArray = require("data/observable-array");
var dialogs = require("ui/dialogs");

var chatList = new observableArray.ObservableArray([]);
var apikey;
var homeViewModel = new HomeViewModel();
var httpModule = require("http");
var observableModule = require("data/observable");
var pageData = new observableModule.Observable();

var dialogs = require("ui/dialogs");
const getFrameById = require("ui/frame").getFrameById;
var createViewModel = require("./home-view-model").createViewModel;
var appSettings = require('application-settings');


function pageLoaded(args) {
  /*
    This gets a reference this page’s <Page> UI component. You can
    view the API reference of the Page to see what’s available at
    https://docs.nativescript.org/api-reference/classes/_ui_page_.page.html
    */
  //var page = args.object;


  if(!appSettings.hasKey('apikey'))
  {
      dialogs.prompt({title:"API KEY",message:"Please enter your API Key",defaultText:"Please get your API key from MessengerPeople console",
      okButtonText:"Ok",
      cancelButtonText:"Cancel"}).then(function(r){
        appSettings.setString('apikey', r.text);
        apikey = r.text;
        //console.log(appSettings.getString('apikey'));
      });

  }
  else
  {
    apikey = appSettings.getString('apikey');
    //console.log(apikey);
  }
  //console.log(apikey);
  
  

  var page = args.object;
  pageData.set("chatList", chatList);
  // it links an xml "beerList" variable to a js beerList variable
  page.bindingContext = { chatList };

  loadchats();

  /*
    A page’s bindingContext is an object that should be used to perform
    data binding between XML markup and JavaScript code. Properties
    on the bindingContext can be accessed using the {{ }} syntax in XML.
    In this example, the {{ message }} and {{ onTap }} bindings are resolved
    against the object returned by createViewModel().

    You can learn more about data binding in NativeScript at
    https://docs.nativescript.org/core-concepts/data-binding.
    */
  //page.bindingContext = homeViewModel;
}
function onItemTap (args) {
  //console.log("Item Tapped");


 /* const navigationEntry = {
    moduleName: "detail-page",
    context: { info: chatList.getItem(args.index) },
    animated: false
  };*/
  const frame = frameModule.topmost();
  


  //const frame = getFrameById;
  // - console.log(chatList.getItem(args.index).user_id);
  // appSettings.setString('selectedid', chatList.getItem(args.index).user_id);

   appSettings.setString('selectedid', args.object.text);


  /*frame.navigate({
    moduleName: "detail-page",
    context: { info: chatList.getItem(args.index) },
    animated: false,
    backstackVisible: false
  });
  const button = args.object;
  const page = button.page;*/
  page.frame.navigate("./detail/detail-page");

}
function onButtonTap (args) {
  //console.log("Button Tapped");
  //dialogs.alert("Refresh Data");

  loadchats();

}

function loadchats()
{

  chatList.length=0;

  httpModule.request({
    url: "https://rest.messengerpeople.com/api/channel/chats?apikey="+apikey+"&limit=50&start=&incoming=1&outgoing=1&bot=0",
    method: "GET"
  }).then((response) => {
    // Content property of the response is HttpContent
    // The toString method allows you to get the response body as string.
    const str = response.content.toString();
    // The toJSON method allows you to parse the received content to JSON object
    var obj = response.content.toJSON();
    chatList.length = 0;
    //console.log(Object.keys(obj["chats"]).length);
    for (i = 0; i < Object.keys(obj["chats"]).length; i++) {
      var chat = { user_id: obj["chats"][i].user_id, chat: obj["chats"][i].chat };


      if(i==0)
      {
        chatList.push(chat);
      }
      else
      {
        /*console.log(i);
        console.log(chatList.getItem(chatList.length-1).user_id);
        console.log(chat.user_id);
        console.log(chatList.length);*/
        /*var chatExists = chatList.getItem.indexOf(chat.user_id,0);
        console.log(findinChatList(chat.user_id));*/
      
       if(findinChatList(chat.user_id) == 0)
        {

            chatList.push(chat);
        }

      }
      
      
    }


    //console.log(chatList.getItem(i).user_id);
    //console.log("chat list count " + chatList.length);
  })

}

function onNavigatingTo(args) {
  page = args.object;   
  page.bindingContext = createViewModel;
}

function findinChatList(userid)
{
  
  for(var i=0;i<chatList.length;i++)
  {
    if(chatList.getItem(i).user_id===userid)
    {
        return 1;
    }
  }
  return 0;
}
function onAskAPI(args)
{

  if(!appSettings.hasKey('apikey'))
  {
    dialogs.prompt({title:"API KEY",message:"Please enter your API Key",defaultText:"Please get your API key from MessengerPeople console",
    okButtonText:"Ok",
    cancelButtonText:"Cancel"}).then(function(r){
        appSettings.setString('apikey', r.text);
        apikey = r.text;
      });

  }
  else
  {
    apikey = appSettings.getString('apikey');
    dialogs.prompt({title:"API KEY",message:"Please enter your API Key",defaultText:apikey,
    okButtonText:"Ok",
    cancelButtonText:"Cancel"}).then(function(r){
        appSettings.setString('apikey', r.text);
        apikey = r.text;
      });
  }

  //console.log("menu tapped")

}
/*
Exporting a function in a NativeScript code-behind file makes it accessible
to the file’s corresponding XML file. In this case, exporting the pageLoaded
function here makes the pageLoaded="pageLoaded" binding in this page’s XML
file work.
*/
exports.pageLoaded = pageLoaded;
exports.onItemTap = onItemTap;
exports.onButtonTap = onButtonTap;
exports.onNavigatingTo = onNavigatingTo;
exports.onAskAPI = onAskAPI;
